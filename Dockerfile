FROM mhart/alpine-node:10

RUN npm i -g yarn

WORKDIR /app

COPY packages/api/build .

COPY packages/api/package.json .
COPY packages/api/src/contribution/delivery/graphql/schema.graphql ./contribution/delivery/graphql/schema.graphql
COPY yarn.lock .

RUN yarn install --prod
COPY packages/core/build ./node_modules/@brickblock/core

EXPOSE 5001
CMD ["node", "index.js"]