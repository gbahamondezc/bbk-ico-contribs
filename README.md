# Brickblock Initial Coin Offering Dashboard

Web + GraphQL Application showing Brickblock Initial Coin Offering contributions
in two different formats (Table, Charts)

### Useful Links

-   [Link to the initial Product specification](https://gitlab.com/gbahamondezc/bbk-ico-contribs/issues/4)

-   [Link to the firebase + GKE deployed Application (manually)](https://graphql-demo-264308.firebaseapp.com/)

*   [Link to the development board](https://gitlab.com/gbahamondezc/bbk-ico-contribs/-/boards)

*   [Pipelines](https://gitlab.com/gbahamondezc/bbk-ico-contribs/pipelines)

### Relevant Libraries

-   [lerna](https://github.com/lerna/lerna) (monorepo)
-   [Apollo server and client](https://www.apollographql.com/docs/apollo-server/)
    (GraphQL)
-   [SequelizeJS](https://sequelize.org/) (PostgreSQL)
-   [Material UI](https://material-ui.com/) Google's Material Design
    implementation library for React.

### Where the contributions data comes from?

It is fetched from [Etherscan](https://etherscan.io/address/brickblock-ico.eth)
using [Etherscan Accounts API](https://etherscan.io/apis#accounts) using
[Axios http library](https://github.com/axios/axios), then stored in a
PostgreSQL database, as etherscan only has Ether transactions, the currency
field was modified randomly to diversify the contributions between ETH, BTC &
LTC
[see more here](https://gitlab.com/gbahamondezc/bbk-ico-contribs/blob/master/packages/api/src/tasks/fetch.contribs.ts)

### Project structure

-   Packages

    -   **@brickblock/core**: Shared package, containing basic bussines logic
        and domain types definitions, used by the other two packages (this could
        be published in some kind of private registry in a real app).

    -   **@brickblock/api**: Koa + Apollo Server app, uses core and implements
        reposotories given the interfaces in core, for retrieve and send
        information about Contributions.

    -   **@brickblock/dashboard**: Front end application, with React, Redux,
        Apollo, Material-ui which fetchs data from API and display information
        as list and a couple of charts, allowing to filter by
        `amount of contribution` and `currencies`

### How to run local

#### Requirements

-   **NodeJS at least 10.x.x**
-   **Docker + docker-compose** (just to run the database)
-   **yarn**

```sh
# clone the repo  and go into the directory
$ git clone git@gitlab.com:gbahamondezc/bbk-ico-contribs.git && cd bbk-ico-contribs

# this will take a couple of minutes
$ yarn bootstrap

# start the postgresql instance
$ docker-compose up -d database

# load the initial data into the database
$ yarn load

# start development, yiis!
$ yarn dev

# Or if you want to run in separated ttys
$ yarn dev:api
$ yarn dev:dashboard

# Tests all packages
$ yarn test

# Test single in jest watch mode
$ yarn test:api
$ yarn test:core
$ yarn test:dashboard
```

### Author questions

-   Is possible to contribute with a Bitcoin or Litecoin wallet to a Ethereum
    based ICO?

-   What role plays Geth, I tried to use
    [Web3js](https://github.com/ethereum/web3.js/) to retrieve the contributions
    information, but I fail absolutely to connect with public Ethereum network,
    Geth is something that can helps here?

-   Make sense to use stuff like gzip, helmet, cors, etc.... middlewares on my
    Koa + GraphQL App or Apollo Middleware takes care about it for me?

### Todo :(

-   [ ] Improve test coverage it's veeeery low
-   [ ] Deploy automatically
-   [ ] Write Typedocs documentation, The package is installed, but, finally I
        run out of time and didn't documented my code, Sorry D:
-   [ ] Feature, Display the dashboard and list converting the currency in the
        currency that you choose from a list of currencies.

### Some Articles/Sites that I fully read during this challenge.

#### Brickblock

-   https://icoholder.com/en/brickblock

-   https://medium.com/@maduekelinus/why-i-should-contribute-to-brickblock-ico-6397cf5ac2c0

-   https://iconow.net/brickblock-ico-is-now-closed-total-result-4-78m/

-   https://medium.com/@vminhtr_42068/brickblock-review-review-and-ico-details-c3953be4348d

https://medium.com/@Brickblock/how-do-brickblock-tokens-bbk-work-68e824228ef1

-   https://medium.com/@tranhuongvina/guide-to-buy-bbk-token-in-ico-brickblock-1190025c23ac

#### Monorepos

-   https://medium.com/@craigcartmell/full-stack-javascript-and-how-i-finally-built-an-api-im-proud-of-53006ae152dc

-   https://github.com/korfuri/awesome-monorepo

-   https://medium.com/hy-vee-engineering/creating-a-monorepo-with-lerna-yarn-workspaces-cf163908965d

#### Crypto converters APIs

-   https://github.com/markpenovich/cryptocurrency-unit-convert#readme

-   https://min-api.cryptocompare.com/documentation

-   https://codesandbox.io/s/48p1r2roz4
