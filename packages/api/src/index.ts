import { Server } from 'net';
import { startApp } from './config/app';

startApp()
    .then((server: Server) => {
        server.on('error', (error: Error) => {
            console.log(error);
        });
    })
    .catch((error: Error) => {
        console.log(error);
    });
