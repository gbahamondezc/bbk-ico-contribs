import { Server } from 'net';
import { createUseCase, ContributionUseCase } from '@brickblock/core';
import { createPGContributionRepository } from '../contribution/repository/postgres';
import { createConnection, ConnectionOptions } from './postgres/connection';

import { createHttpGraphQLServer, HttpServerOptions } from './http/server';

import { importSchema } from 'graphql-import';

export interface AppUseCases {
    contributionUseCase?: ContributionUseCase;
}

const postgresOptions: ConnectionOptions = {
    host: process.env.POSTGRES_HOST as string,
    port: Number(process.env.POSTGRES_PORT),
    username: process.env.POSTGRES_USERNAME as string,
    password: process.env.POSTGRES_PASSWORD as string,
    database: process.env.POSTGRES_DATABASE as string,
    recreate: false,
};

const httpServerOptions: HttpServerOptions = {
    port: Number(process.env.GRAPHQL_SERVER_PORT),
    debug: true,
};

export const startApp = async (): Promise<Server> => {
    const pgConnection = await createConnection(postgresOptions);
    const graphQLSchema = await importSchema(
        `${__dirname}/../contribution/delivery/graphql/schema.graphql`,
    );
    return createHttpGraphQLServer({
        graphQLSchema,
        httpServerOptions,
        useCases: {
            contributionUseCase: createUseCase(
                createPGContributionRepository(pgConnection),
            ),
        },
    });
};
