import { AppUseCases } from '../app';

import { Server } from 'net';
import Koa from 'koa';
import logger from 'koa-logger';

import { ApolloServer } from 'apollo-server-koa';

import { createContributionResolvers } from '../../contribution/delivery/graphql/resolvers';

export interface HttpServerOptions {
    debug: boolean;
    port?: number;
}

export interface HttpGraphQLServerConfig {
    useCases: AppUseCases;
    httpServerOptions: HttpServerOptions;
    graphQLSchema: string;
}

export const createHttpGraphQLServer = (
    config: HttpGraphQLServerConfig,
): Server => {
    const server = new ApolloServer({
        typeDefs: config.graphQLSchema,
        resolvers: createContributionResolvers(),
        context: {
            useCase: config.useCases.contributionUseCase,
        },
    });

    const app = new Koa();

    app.use(logger());

    server.applyMiddleware({ app });

    return app.listen(config.httpServerOptions.port, () => {
        console.log(
            `🚀 Server running at http://127.0.0.1:${config.httpServerOptions.port}`,
        );
    });
};
