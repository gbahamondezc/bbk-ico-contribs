import { Sequelize } from 'sequelize-typescript';
import { ContributionModel } from '../../contribution/repository/postgres';

export interface ConnectionOptions {
    host: string;
    username: string;
    password: string;
    port: number;
    database: string;
    recreate?: boolean;
}

export interface SequelizeConnection {
    sequelize: Sequelize;
    Contribution: typeof ContributionModel;
}

export const createConnection = async (
    options: ConnectionOptions,
): Promise<SequelizeConnection> => {
    const sequelize = new Sequelize({
        ...options,
        dialect: 'postgres',
        models: [ContributionModel],
    });

    await sequelize.authenticate();

    if (options.recreate) {
        await sequelize.sync({
            force: true,
        });
    }

    return {
        sequelize,
        Contribution: ContributionModel,
    };
};
