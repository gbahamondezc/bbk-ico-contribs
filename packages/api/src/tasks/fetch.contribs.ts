import { createUseCase, ContributionRepository } from '@brickblock/core';

import {
    createConnection,
    ConnectionOptions,
} from '../config/postgres/connection';

import { createPGContributionRepository } from '../contribution/repository/postgres';

import {
    createEtherscanRepository,
    EtherscanOptions,
} from '../contribution/repository/etherscan/repository';

const options: ConnectionOptions = {
    host: process.env.POSTGRES_HOST as string,
    port: Number(process.env.POSTGRES_PORT),
    username: process.env.POSTGRES_USERNAME as string,
    password: process.env.POSTGRES_PASSWORD as string,
    database: process.env.POSTGRES_DATABASE as string,
    recreate: true,
};

const etherscanOptions: EtherscanOptions = {
    apiKey: process.env.ETHERSCAN_API_KEY as string,
    address: process.env.ETHEREUM_ADDRESS as string,
};

(async (): Promise<void> => {
    try {
        const connection = await createConnection(options);
        const postgresRepository = createPGContributionRepository(connection);
        const etherscanRepository = createEtherscanRepository(etherscanOptions);

        const ethscanContribUseCase = createUseCase(
            etherscanRepository as ContributionRepository,
        );

        const pgContribUseCase = createUseCase(postgresRepository);

        const contribs = await ethscanContribUseCase.listByFilter();

        await pgContribUseCase.save!(contribs);
    } catch (error) {
        console.log(error);
    }
})();
