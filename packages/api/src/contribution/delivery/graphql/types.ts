/* eslint-disable @typescript-eslint/no-explicit-any */
import {
    Contribution,
    ContributionUseCase,
    ContributionFilters,
    CountributionGroup,
} from '@brickblock/core';

import { IResolvers } from 'apollo-server-koa';

export interface Context {
    useCase: ContributionUseCase;
}

export type ContributorResolverFunction = (
    parent: any,
    args: ContributionFilters,
    ctx: Context,
) => Promise<Contribution[] | CountributionGroup[]>;

export interface ResolverMap {
    [field: string]: ContributorResolverFunction;
}

export interface ContributionResolvers extends IResolvers {
    Query: ResolverMap;
}
