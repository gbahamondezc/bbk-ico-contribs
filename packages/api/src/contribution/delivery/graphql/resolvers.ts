/* eslint-disable @typescript-eslint/no-explicit-any */

import {
    Contribution,
    ContributionFilters,
    CountributionGroup,
} from '@brickblock/core';

import { Context, ContributionResolvers } from './types';

export const createContributionResolvers = (): ContributionResolvers => ({
    Query: {
        getICOContributions: async (
            _: any,
            args: ContributionFilters,
            ctx: Context,
        ): Promise<Contribution[]> => {
            console.log(args);
            return ctx.useCase.listByFilter(args);
        },

        getICOContributionsGroupedByCurrency: async (
            _: any,
            args: ContributionFilters,
            ctx: Context,
        ): Promise<CountributionGroup[]> => {
            return ctx.useCase.listGroupsByCurrency(args);
        },
    },
});
