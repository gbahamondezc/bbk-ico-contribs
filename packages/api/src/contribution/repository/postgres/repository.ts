import {
    ContributionRepository,
    ContributionFilters,
    Contribution,
    CountributionGroup,
} from '@brickblock/core';

import { SequelizeConnection } from '../../../config/postgres/connection';

import { Op, fn, col } from 'sequelize';

export const createPGContributionRepository = (
    conn: SequelizeConnection,
): ContributionRepository => ({
    fetch: async (filter: ContributionFilters): Promise<Contribution[]> => {
        return conn.Contribution.findAll({
            where: {
                [Op.and]: [
                    {
                        currency: {
                            [Op.in]: filter.currencies,
                        },
                    },
                    {
                        amount: {
                            [Op.between]: filter.amountRange,
                        },
                    },
                ],
            },
        });
    },

    fetchGroupedByCurrency: async (
        filter: ContributionFilters,
    ): Promise<CountributionGroup[]> => {
        const result = await conn.Contribution.findAll({
            attributes: {
                include: [
                    [fn(filter.operation as string, col('amount')), 'total'],
                ],
                exclude: ['id', 'address', 'txid', 'amount'],
            },
            where: {
                [Op.and]: [
                    {
                        currency: {
                            [Op.in]: filter.currencies,
                        },
                    },
                    {
                        amount: {
                            [Op.between]: filter.amountRange,
                        },
                    },
                ],
            },
            group: ['currency'],
            raw: true,
        });

        // Ugly casting due to sequelize poor types :(
        return (result as unknown) as CountributionGroup[];
    },

    batchCreate: async (
        contributions: Contribution[],
    ): Promise<Contribution[]> => {
        return conn.Contribution.bulkCreate(contributions);
    },
});
