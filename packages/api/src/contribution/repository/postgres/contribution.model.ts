import { Contribution, Currency } from '@brickblock/core';

import {
    Table,
    Column,
    Model,
    PrimaryKey,
    DataType,
    AutoIncrement,
} from 'sequelize-typescript';

@Table({
    timestamps: false,
    tableName: 'contribution',
})
export class ContributionModel extends Model<ContributionModel>
    implements Contribution {
    @PrimaryKey
    @AutoIncrement
    @Column
    id!: number;

    @Column
    address!: string;

    @Column({
        type: DataType.DECIMAL,
    })
    amount!: number;

    @Column({
        type: DataType.ENUM(
            Currency.Bitcoin,
            Currency.Ether,
            Currency.Litecoin,
        ),
    })
    currency!: Currency;

    @Column
    txid!: string;
}
