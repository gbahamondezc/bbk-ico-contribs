import { Contribution, Currency } from '@brickblock/core';
import { EtherscanTransaction } from './repository';
import { chain as flatMap } from 'ramda';

const currencies: Currency[] = [
    Currency.Bitcoin,
    Currency.Litecoin,
    Currency.Ether,
];

export const randomCurrency = (): Currency =>
    currencies[Math.floor(Math.random() * currencies.length)];

export const mapToContributions = (
    address: string,
    transactions: EtherscanTransaction[],
): Contribution[] =>
    flatMap(
        (transact: EtherscanTransaction) =>
            transact.from !== address
                ? [
                      {
                          address: transact.from,
                          currency: randomCurrency(),
                          amount: Number(transact.value),
                          txid: transact.hash,
                      },
                  ]
                : [],
        transactions,
    );
