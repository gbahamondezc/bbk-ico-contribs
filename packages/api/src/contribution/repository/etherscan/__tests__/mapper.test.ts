import { Currency } from '@brickblock/core';
import { mapToContributions, randomCurrency } from '../mapper';
import { times } from 'ramda';

import transactions from './transactions';

describe('Etherscan repository Mapper', () => {
    it('Must map an array of Etherscan transaction to a list of Contributions', () => {
        const address = '0xc7f4ed68592327aa2755b320c353b1af715020dd';
        expect(mapToContributions(address, transactions)).toHaveLength(2);
    });

    it('Must return a random currency from the available currencies list', () => {
        const currencies = [
            Currency.Bitcoin,
            Currency.Litecoin,
            Currency.Ether,
        ];
        times(
            () => expect(currencies.includes(randomCurrency())).toBeTruthy(),
            20,
        );
    });
});
