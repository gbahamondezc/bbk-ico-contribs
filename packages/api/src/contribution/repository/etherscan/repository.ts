import { ContributionRepository, Contribution } from '@brickblock/core';

import http, { AxiosResponse } from 'axios';
import { mapToContributions } from './mapper';

const APIUri = 'http://api.etherscan.io/api';

export interface EtherscanOptions {
    address: string;
    apiKey: string;
}

export interface EtherscanResponse {
    result: EtherscanTransaction[];
}

export interface EtherscanTransaction {
    blockNumber: string;
    timeStamp: string;
    hash: string;
    nonce: string;
    blockHash: string;
    transactionIndex: string;
    from: string;
    to: string;
    value: string;
    gas: string;
    gasPrice: string;
    isError: string;
    txreceipt_status: string;
    input: string;
    contractAddress: string;
    cumulativeGasUsed: string;
    gasUsed: string;
    confirmations: string;
}

export const createEtherscanRepository = (
    options: EtherscanOptions,
): Partial<ContributionRepository> => ({
    fetch: async (): Promise<Contribution[]> => {
        const httpResponse: AxiosResponse = await http.get(
            `${APIUri}?module=account&action=txlist&address=${options.address}&startblock=0&endblock=99999999&sort=asc&apiKey=${options.apiKey}`,
        );

        const response: EtherscanResponse = httpResponse.data;

        return mapToContributions(options.address, response.result);
    },
});
