/* eslint-disable @typescript-eslint/no-explicit-any */
import { ContributionFilters, CountributionGroup } from '@brickblock/core';

export enum ContributionGroupActionType {
    GetContributionsGrouped = 'GET_CONTRIBUTIONS_GROUPED',
    GetContributionsGroupedSumSucceed = 'GET_CONTRIBUTIONS_GROUPED_SUM_SUCCEEDED',
    GetContributionsGroupedCountSucceed = 'GET_CONTRIBUTIONS_GROUPED_COUNT_SUCCEEDED',
    GetContributionsGroupedSumFailed = 'GET_CONTRIBUTIONS_GROUPED_SUM_FAILED',
    GetContributionsGroupedCountFailed = 'GET_CONTRIBUTIONS_GROUPED_COUNT_FAILED',
}

export interface ContributionGroupAction {
    type: ContributionGroupActionType;
    payload?: any;
}

export const actions = {
    getContributionsGrouped: (
        filters: ContributionFilters,
    ): ContributionGroupAction => ({
        type: ContributionGroupActionType.GetContributionsGrouped,
        payload: filters,
    }),
    getContributionsGroupedCountSuccess: (
        response: any,
    ): ContributionGroupAction => ({
        type: ContributionGroupActionType.GetContributionsGroupedCountSucceed,
        payload: response,
    }),
    getContributionsGroupedSumSuccess: (
        response: any,
    ): ContributionGroupAction => ({
        type: ContributionGroupActionType.GetContributionsGroupedSumSucceed,
        payload: response,
    }),
};

export interface GroupsBy {
    sum?: CountributionGroup[];
    count?: CountributionGroup[];
}

const contributionReducers = (
    state: GroupsBy = {},
    action: ContributionGroupAction,
): GroupsBy => {
    switch (action.type) {
        case ContributionGroupActionType.GetContributionsGroupedSumSucceed:
            return {
                ...state,
                sum: action.payload,
            };
        case ContributionGroupActionType.GetContributionsGroupedCountSucceed:
            return {
                ...state,
                count: action.payload,
            };
        default:
            return state;
    }
};

export default contributionReducers;
