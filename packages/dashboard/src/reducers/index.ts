import { combineReducers } from 'redux';
import contributions from './contributions';
import contributionGroups from './contributionGroups';

export default combineReducers({
    contributions,
    contributionGroups,
});
