/* eslint-disable @typescript-eslint/no-explicit-any */
import { Contribution, ContributionFilters } from '@brickblock/core';

export enum ContributionActionType {
    GetContributions = 'GET_CONTRIBUTIONS',
    GetContributionsSuccess = 'GET_CONTRIBUTIONS_SUCCEEDED',
    GetContributionsFailed = 'GET_CONTRIBUTIONS_FAILED',
}

export interface ContributionAction {
    type: ContributionActionType;
    payload?: any;
}

export const actions = {
    getContributions: (filters: ContributionFilters): ContributionAction => ({
        type: ContributionActionType.GetContributions,
        payload: filters,
    }),
    getContributionsSuccess: (response: any): ContributionAction => ({
        type: ContributionActionType.GetContributionsSuccess,
        payload: response,
    }),
};

const contributionReducers = (
    state: Contribution[] = [],
    action: ContributionAction,
): Contribution[] => {
    switch (action.type) {
        case ContributionActionType.GetContributionsSuccess:
            return action.payload;
        default:
            return state;
    }
};

export default contributionReducers;
