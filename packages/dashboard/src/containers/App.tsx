import React from 'react';
import { withConfig } from '../withConfig';
import { useStyles } from './types';

import Home from './Home';

const App = (): JSX.Element => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Home />
        </div>
    );
};

export default withConfig(App);
