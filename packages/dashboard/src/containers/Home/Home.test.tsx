import React from 'react';
import { ShallowWrapper } from 'enzyme';
import createShallow from '@material-ui/core/test-utils/createShallow';
import Home from './Home';
import { withConfig } from '../../withConfig';

describe('Testing Home Component basics', () => {
    it('renders without styles', () => {
        const shallow = createShallow();
        const ConfigHome = withConfig(Home);
        const wrapper: ShallowWrapper = shallow(<ConfigHome />);
        expect(wrapper).toBeTruthy();
    });
});
