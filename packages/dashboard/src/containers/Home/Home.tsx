import { Currency, Operation, CountributionGroup } from '@brickblock/core';
import React, { useState, useEffect, ChangeEvent } from 'react';

import { useSelector, useDispatch } from 'react-redux';

import { Tabs, Tab } from '@material-ui/core';
import { List as ListIcon, PieChart as PieChartIcon } from '@material-ui/icons';

import ICOContribTable from '../../components/ICOContribTable';
import ICOContribDashboard from '../../components/ICOContribDashboard';
import { ChartUnit } from '../../components/ICOContribDashboard/types';
import TabPanel from '../../components/TabPanel';
import Filters from '../../components/Filters';

import { AppState } from '../../store/configureStore';

import { actions as contribActions } from '../../reducers/contributions';
import { actions as contribGroupActions } from '../../reducers/contributionGroups';

import { useStyles, Props } from './types';

const groupsToDashboardInput = (group?: CountributionGroup[]): ChartUnit[] => {
    if (!group) {
        return [];
    }
    const colors = {
        [Currency.Ether]: '#2a61ff',
        [Currency.Bitcoin]: '#f2a900',
        [Currency.Litecoin]: '#00aeff',
    };

    return group.map((group: CountributionGroup) => ({
        name: group.currency,
        value: group.total,
        color: colors[group.currency],
    }));
};

const Home = (props: Props): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [activeTab, setActiveTab] = useState(0);

    const contributions = useSelector((state: AppState) => state.contributions);
    const sumGroup = useSelector(
        (state: AppState) => state.contributionGroups.sum,
    );

    const countGroup = useSelector(
        (state: AppState) => state.contributionGroups.count,
    );

    const currencies: Currency[] = [
        Currency.Bitcoin,
        Currency.Litecoin,
        Currency.Ether,
    ];

    const [selectedCurrencies, setSelectedCurrencies] = useState(currencies);
    const [amountRangeValue, setAmountRangeValue] = useState<number[]>([
        0,
        2000,
    ]);

    const handleChangeCurrencies = (
        event: ChangeEvent<{ value: unknown }>,
    ): void => {
        setSelectedCurrencies(event.target.value as Currency[]);
    };

    const handleChangeAmountRange = (
        event: unknown,
        newValue: number | number[],
    ): void => {
        setAmountRangeValue(newValue as number[]);
    };

    useEffect(() => {
        dispatch(
            contribActions.getContributions({
                currencies: selectedCurrencies,
                amountRange: amountRangeValue,
            }),
        );

        dispatch(
            contribGroupActions.getContributionsGrouped({
                currencies: selectedCurrencies,
                amountRange: amountRangeValue,
                operation: Operation.Count,
            }),
        );

        dispatch(
            contribGroupActions.getContributionsGrouped({
                currencies: selectedCurrencies,
                amountRange: amountRangeValue,
                operation: Operation.Sum,
            }),
        );
    }, [selectedCurrencies, amountRangeValue, dispatch]);

    const changeActiveTab = (
        event: React.ChangeEvent<{}>,
        newValue: number,
    ): void => {
        setActiveTab(newValue);
    };

    return (
        <div className={classes.root}>
            <Tabs
                value={activeTab}
                onChange={changeActiveTab}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="secondary"
            >
                <Tab icon={<ListIcon />} label="List"></Tab>
                <Tab icon={<PieChartIcon />} label="Chart"></Tab>
            </Tabs>
            <Filters
                handleChangeCurrencies={handleChangeCurrencies}
                handleChangeAmountRange={handleChangeAmountRange}
                selectedCurrencies={selectedCurrencies}
                amountRangeValue={amountRangeValue}
            />
            <TabPanel value={activeTab} index={0}>
                <ICOContribTable contributions={contributions} />
            </TabPanel>
            <TabPanel value={activeTab} index={1}>
                <ICOContribDashboard
                    transactionsByCurrency={{
                        data: groupsToDashboardInput(countGroup),
                    }}
                    amountByCurrency={{
                        name: 'Currency',
                        data: groupsToDashboardInput(sumGroup),
                    }}
                />
            </TabPanel>
        </div>
    );
};

export default Home;
