import { makeStyles } from '@material-ui/core';

export interface Props {}

export const useStyles = makeStyles(theme => ({
    root: {
        paddingLeft: 10,
    },
}));
