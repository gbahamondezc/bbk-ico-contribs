import { makeStyles } from '@material-ui/core';

export interface Props {}

export const useStyles = makeStyles(theme => ({
    flex: {
        flex: 1,
    },
    root: {
        flexGrow: 1,
    },
}));
