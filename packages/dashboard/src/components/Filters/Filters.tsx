import React from 'react';
import { Currency } from '@brickblock/core';

import {
    Grid,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Typography,
    Slider,
    Input,
    Chip,
} from '@material-ui/core';

import { Props, useStyles } from './types';

const Filter = (props: Props): JSX.Element => {
    const classes = useStyles();
    const currencies: Currency[] = [
        Currency.Bitcoin,
        Currency.Litecoin,
        Currency.Ether,
    ];

    const valuetext = (value: number): string => {
        return `${value}`;
    };

    return (
        <Grid className={classes.root} container>
            <Grid item xs={12} md={3}>
                <FormControl className={classes.formControl}>
                    <InputLabel id="currency-selector">Currency</InputLabel>
                    <Select
                        id="currency-selector"
                        labelId="currency-selector-select"
                        multiple
                        value={props.selectedCurrencies}
                        onChange={props.handleChangeCurrencies}
                        input={<Input id="currency-selector-input" />}
                        renderValue={(selected: unknown): JSX.Element => (
                            <div className={classes.chips}>
                                {(selected as string[]).map(value => (
                                    <Chip key={value} label={value} />
                                ))}
                            </div>
                        )}
                    >
                        {currencies.map((name: string) => (
                            <MenuItem key={name} value={name}>
                                {name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={12} md={3}>
                <Typography id="amount-slider" gutterBottom>
                    Amount Range (Ether)
                </Typography>
                <Slider
                    className={classes.slider}
                    value={props.amountRangeValue}
                    onChange={props.handleChangeAmountRange}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    step={10}
                    max={2000}
                    min={0}
                ></Slider>
            </Grid>
            <Grid item xs={12} md={6}>
                {' '}
            </Grid>
        </Grid>
    );
};

export default Filter;
