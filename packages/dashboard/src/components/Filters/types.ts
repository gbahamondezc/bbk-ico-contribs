import { ChangeEvent } from 'react';
import { makeStyles } from '@material-ui/core';
import { Currency } from '@brickblock/core';

export interface Props {
    handleChangeCurrencies(event: ChangeEvent<{ value: unknown }>): void;
    handleChangeAmountRange(event: unknown, newValue: number | number[]): void;
    selectedCurrencies: Currency[];
    amountRangeValue: number[];
}

export const useStyles = makeStyles(theme => ({
    root: {
        marginTop: 50,
        marginBottom: 20,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        marginLeft: '8%',
        minWidth: '80%',
    },
    slider: {
        marginTop: 16,
    },
}));
