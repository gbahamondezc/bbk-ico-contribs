import { makeStyles } from '@material-ui/core';
import { DonutChartProps } from './DonutChart';
import { TopChartProps } from './TopChart';

export interface Props {
    transactionsByCurrency: DonutChartProps;
    amountByCurrency: TopChartProps;
}

export interface ChartUnit {
    name: string;
    value: number;
    color: string;
}

export const useStyles = makeStyles(theme => ({
    chartWrapper: {
        display: 'flex',
        justifyContent: 'center',
        paddingTop: 10,
    },
}));
