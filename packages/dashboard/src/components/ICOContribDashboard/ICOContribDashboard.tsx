import React from 'react';
import { useStyles, Props } from './types';
import DonutChart from './DonutChart';
import TopChart from './TopChart';

import { Grid, Paper } from '@material-ui/core';

const ICOContribDashboard = (props: Props): JSX.Element => {
    const classes = useStyles();
    return (
        <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
                <Paper className={classes.chartWrapper}>
                    <DonutChart data={props.transactionsByCurrency.data} />
                </Paper>
            </Grid>
            <Grid item xs={12} md={6}>
                <Paper className={classes.chartWrapper}>
                    <TopChart
                        data={props.amountByCurrency.data}
                        name={props.amountByCurrency.name}
                    />
                </Paper>
            </Grid>
        </Grid>
    );
};

export default ICOContribDashboard;
