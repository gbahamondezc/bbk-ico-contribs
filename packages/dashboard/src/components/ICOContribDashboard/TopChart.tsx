/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { ChartUnit } from './types';

import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
} from 'recharts';

export interface TopChartProps {
    name: string;
    data: ChartUnit[];
}

const TopChart = (props: TopChartProps): JSX.Element => {
    const values = props.data.reduce((accum: any, element: any) => {
        accum[element.name] = element.value;
        return accum;
    }, {});

    const data = {
        name: props.name,
        ...values,
    };

    return (
        <BarChart
            width={600}
            height={400}
            data={[data]}
            margin={{
                top: 20,
                right: 30,
                left: 20,
                bottom: 5,
            }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            {props.data.map((unit: ChartUnit) => (
                <Bar key={unit.name} dataKey={unit.name} fill={unit.color} />
            ))}
        </BarChart>
    );
};

export default TopChart;
