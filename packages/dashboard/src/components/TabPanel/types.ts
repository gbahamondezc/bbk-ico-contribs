export interface Props {
    children?: React.ReactNode;
    index: number;
    value: number;
}
