import { makeStyles } from '@material-ui/core';
import { Contribution } from '@brickblock/core';

export interface Props {
    contributions: Contribution[];
}

export const useStyles = makeStyles(theme => ({}));
