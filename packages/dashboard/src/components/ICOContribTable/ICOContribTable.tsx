import React, { useState } from 'react';
import { Props } from './types';

import {
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    TableContainer,
    Grid,
    TablePagination,
    Paper,
} from '@material-ui/core';

import { Contribution } from '@brickblock/core';

const ICOContribTable = (props: Props): JSX.Element => {
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);

    const handleChangePage = (event: unknown, newPage: number): void => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement>,
    ): void => {
        setRowsPerPage(parseInt(event.target.value, 10));
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Paper>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">
                                        Address
                                    </TableCell>
                                    <TableCell align="center">
                                        Currency
                                    </TableCell>
                                    <TableCell align="right">Amount</TableCell>
                                    <TableCell align="right">
                                        Transaction ID
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {props.contributions
                                    .slice(
                                        page * rowsPerPage,
                                        page * rowsPerPage + rowsPerPage,
                                    )
                                    .map((contribution: Contribution) => {
                                        const id = `contrib-${contribution.id}`;
                                        return (
                                            <TableRow
                                                hover
                                                tabIndex={-1}
                                                key={id}
                                            >
                                                <TableCell
                                                    component="th"
                                                    id={id}
                                                >
                                                    {contribution.address}
                                                </TableCell>
                                                <TableCell
                                                    align="center"
                                                    component="th"
                                                    id={id}
                                                >
                                                    {contribution.currency}
                                                </TableCell>
                                                <TableCell
                                                    align="right"
                                                    component="th"
                                                    id={id}
                                                >
                                                    {contribution.amount}
                                                </TableCell>
                                                <TableCell
                                                    align="right"
                                                    component="th"
                                                    id={id}
                                                >
                                                    {contribution.txid}
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 30]}
                        component="div"
                        count={props.contributions.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Paper>
            </Grid>
        </Grid>
    );
};

export default ICOContribTable;
