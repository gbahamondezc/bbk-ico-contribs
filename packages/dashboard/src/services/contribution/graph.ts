/* eslint-disable @typescript-eslint/no-explicit-any */

import ApolloClient, { gql, ApolloQueryResult } from 'apollo-boost';
import { ContributionFilters } from '@brickblock/core';

const client = new ApolloClient({
    uri: process.env.REACT_APP_GRAPHQL_ENDPOINT,
});

export interface GraphQLContributionService {
    getICOContributions(
        filters: ContributionFilters,
    ): Promise<ApolloQueryResult<any>>;

    getICOContributionsGroupedByCurrency(
        filters: ContributionFilters,
    ): Promise<ApolloQueryResult<any>>;
}

const etherToWei = (ether: number): number => {
    const wei = Math.pow(10, 18);
    return wei * ether;
};

const mapFilters = (filters: ContributionFilters): ContributionFilters => {
    if (filters.currencies && filters.amountRange) {
        return {
            currencies: filters.currencies,
            amountRange: [
                etherToWei(filters.amountRange[0]),
                etherToWei(filters.amountRange[1]),
            ],
            operation: filters.operation,
        };
    } else {
        return {};
    }
};

export const createGraphQLContribService = (): GraphQLContributionService => ({
    getICOContributions: (
        filters: ContributionFilters,
    ): Promise<ApolloQueryResult<any>> => {
        const query = gql`
            query ListICOContribs(
                $currencies: [Currency]
                $amountRange: [Float]
            ) {
                getICOContributions(
                    currencies: $currencies
                    amountRange: $amountRange
                ) {
                    id
                    address
                    amount
                    currency
                    txid
                }
            }
        `;

        return client.query({
            query,
            variables: mapFilters(filters),
        });
    },

    getICOContributionsGroupedByCurrency: (
        filters: ContributionFilters,
    ): Promise<ApolloQueryResult<any>> => {
        const query = gql`
            query GroupICOContribs(
                $currencies: [Currency]
                $amountRange: [Float]
                $operation: Operation
            ) {
                getICOContributionsGroupedByCurrency(
                    currencies: $currencies
                    amountRange: $amountRange
                    operation: $operation
                ) {
                    currency
                    total
                }
            }
        `;
        return client.query({
            query,
            variables: mapFilters(filters),
        });
    },
});
