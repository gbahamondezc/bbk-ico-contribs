/* eslint-disable @typescript-eslint/no-explicit-any */

import { all, fork } from 'redux-saga/effects';

import { contributionSaga } from './contribution';
import { contributionGroupSaga } from './contributionGroups';

export default function* rootSaga(): IterableIterator<any> {
    yield all([fork(contributionSaga)]);
    yield all([fork(contributionGroupSaga)]);
}
