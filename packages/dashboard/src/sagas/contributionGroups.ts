/* eslint-disable @typescript-eslint/no-explicit-any */

import { call, takeEvery, put } from 'redux-saga/effects';
import { Operation } from '@brickblock/core';

import {
    ContributionGroupActionType,
    ContributionGroupAction,
    actions,
} from '../reducers/contributionGroups';

import { createGraphQLContribService } from '../services/contribution/graph';

const graphQLContribService = createGraphQLContribService();

function* getContributionsGrouped(
    action: ContributionGroupAction,
): IterableIterator<any> {
    try {
        const response: any = yield call(
            graphQLContribService.getICOContributionsGroupedByCurrency,
            action.payload,
        );

        if (action.payload.operation === Operation.Sum) {
            yield put(
                actions.getContributionsGroupedSumSuccess(
                    response.data.getICOContributionsGroupedByCurrency,
                ),
            );
        }

        if (action.payload.operation === Operation.Count) {
            yield put(
                actions.getContributionsGroupedCountSuccess(
                    response.data.getICOContributionsGroupedByCurrency,
                ),
            );
        }
    } catch (error) {
        console.log(error);
    }
}

export function* contributionGroupSaga(): IterableIterator<any> {
    yield takeEvery(
        ContributionGroupActionType.GetContributionsGrouped,
        getContributionsGrouped,
    );
}
