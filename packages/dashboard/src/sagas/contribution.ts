/* eslint-disable @typescript-eslint/no-explicit-any */

import { call, takeEvery, put } from 'redux-saga/effects';
import {
    ContributionActionType,
    ContributionAction,
    actions,
} from '../reducers/contributions';
import { createGraphQLContribService } from '../services/contribution/graph';

const graphQLContribService = createGraphQLContribService();

function* getContributions(action: ContributionAction): IterableIterator<any> {
    try {
        const response: any = yield call(
            graphQLContribService.getICOContributions,
            action.payload,
        );

        yield put(
            actions.getContributionsSuccess(response.data.getICOContributions),
        );
    } catch (error) {
        console.log(error);
    }
}

export function* contributionSaga(): IterableIterator<any> {
    yield takeEvery(ContributionActionType.GetContributions, getContributions);
}
