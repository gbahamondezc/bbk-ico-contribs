import React from 'react';
import { Provider } from 'react-redux';

import { MuiThemeProvider, CssBaseline } from '@material-ui/core';

import theme from './theme';
import { configureStore, AppState } from './store/configureStore';

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';

const graphClient = new ApolloClient({
    uri: 'http://127.0.0.1:5001/graphql',
});

const store = configureStore({} as AppState);

export const withConfig = (
    Component: React.ComponentType,
): React.ComponentType => {
    return (props: object): JSX.Element => (
        <Provider store={store}>
            <MuiThemeProvider theme={theme}>
                <CssBaseline />
                <ApolloProvider client={graphClient}>
                    <Component {...props} />
                </ApolloProvider>
            </MuiThemeProvider>
        </Provider>
    );
};
