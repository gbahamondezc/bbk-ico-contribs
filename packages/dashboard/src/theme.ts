import { cyan, red } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

const primaryColor = cyan;

export default createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            dark: primaryColor[300],
            light: red.A200,
            main: primaryColor[200],
        },
        secondary: {
            dark: red.A700,
            light: '#ffffff',
            main: '#ffffff',
        },
    },
    typography: {
        fontFamily: 'Lato, sans-serif',
    },
});
