import { applyMiddleware, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import { GroupsBy } from '../reducers/contributionGroups';
import { Contribution } from '@brickblock/core';

import createSagaMiddleware from 'redux-saga';

import rootReducer from '../reducers';
import rootSaga from '../sagas';

export interface AppState {
    contributions: Contribution[];
    contributionGroups: GroupsBy;
}

const sagaMiddleware = createSagaMiddleware();

export const configureStore = (initialState: AppState): Store => {
    const store: Store = createStore(
        rootReducer,
        initialState,
        composeWithDevTools(
            applyMiddleware(reduxImmutableStateInvariant(), sagaMiddleware),
        ),
    );

    sagaMiddleware.run(rootSaga);

    return store;
};
