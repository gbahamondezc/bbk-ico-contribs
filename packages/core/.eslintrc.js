module.exports = {
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint'],
    rules: {
        'no-empty-function': 'off',
        '@typescript-eslint/no-empty-function': 'error',
        '@typescript-eslint/no-non-null-assertion': 'off',
    },
    extends: ['plugin:@typescript-eslint/recommended'],
};
