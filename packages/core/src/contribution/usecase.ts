import {
    ContributionFilters,
    Contribution,
    ContributionRepository,
    CountributionGroup,
} from './types';

export interface ContributionUseCase {
    listByFilter(filter?: ContributionFilters): Promise<Contribution[]>;
    listGroupsByCurrency(
        filter?: ContributionFilters,
    ): Promise<CountributionGroup[]>;
    save(contribution: Contribution[]): Promise<Contribution[]>;
}

export const createUseCase = (
    repository: ContributionRepository,
): ContributionUseCase => ({
    listByFilter: async (
        filter?: ContributionFilters,
    ): Promise<Contribution[]> => {
        return repository.fetch(filter);
    },

    listGroupsByCurrency: async (
        filter?: ContributionFilters,
    ): Promise<CountributionGroup[]> => {
        return repository.fetchGroupedByCurrency(filter);
    },

    save: async (contribs: Contribution[]): Promise<Contribution[]> => {
        return repository.batchCreate(contribs);
    },
});
