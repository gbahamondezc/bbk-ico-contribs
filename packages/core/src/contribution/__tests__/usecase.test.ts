import {
    Contribution,
    ContributionRepository,
    ContributionFilters,
    Currency,
} from '../types';

import { ContributionUseCase, createUseCase } from '../usecase';

describe('Contribution', () => {
    let mockRepository: ContributionRepository;

    let useCase: ContributionUseCase;

    beforeEach(() => {
        mockRepository = {
            fetch: jest.fn(),
            batchCreate: jest.fn(),
            fetchGroupedByCurrency: jest.fn(),
        };

        useCase = createUseCase(mockRepository);
    });

    describe('ICO Contribution', () => {
        it('List ICO Contributions must call respective repo method with given filters', async () => {
            const filters: ContributionFilters = {
                amountRange: [100, 200],
                currencies: [Currency.Bitcoin, Currency.Ether],
            };

            await useCase.listByFilter(filters);

            expect(mockRepository.fetch).toHaveBeenLastCalledWith(filters);
        });

        it('Save ICO Contributions must call respective repo with given contrib list', async () => {
            const newContribs: Contribution[] = [
                {
                    address: 'abc123',
                    amount: 1,
                    currency: Currency.Bitcoin,
                    txid: 'imsometrassactionid',
                },
            ];

            await useCase.save(newContribs);

            expect(mockRepository.batchCreate).toHaveBeenCalledWith(
                newContribs,
            );
        });
    });
});
