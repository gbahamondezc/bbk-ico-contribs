export enum Currency {
    Bitcoin = 'BTC',
    Ether = 'ETH',
    Litecoin = 'LTC',
}

export interface Contribution {
    id?: number;
    address: string;
    currency: Currency;
    amount: number;
    txid: string;
}

export interface CountributionGroup {
    currency: Currency;
    total: number;
}

export enum Operation {
    Sum = 'SUM',
    Count = 'COUNT',
}

export interface ContributionFilters {
    currencies?: Currency[];
    amountRange?: number[];
    operation?: Operation;
}

export interface ContributionRepository {
    fetch(filter?: ContributionFilters): Promise<Contribution[]>;
    fetchGroupedByCurrency(
        filter?: ContributionFilters,
    ): Promise<CountributionGroup[]>;
    batchCreate(contributions: Contribution[]): Promise<Contribution[]>;
}
